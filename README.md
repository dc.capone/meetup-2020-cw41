# LA / SoCal / Orange County Meetup 2020

## Resources

- [Event](https://www.meetup.com/Orange-County-GitLab-Users-Group/events/273577534/)
- [Slides](https://docs.google.com/presentation/d/1ajDgQfqNI2TclSpK3DzM8FqNXPSKYHupsDyI3W9bNyo/edit?usp=sharing)

The slides provide the step-by-step instructions as exercises for this repository.
